import logging
import os
from pathlib import Path


def check_cache_dir():
    path = get_cache_dir()
    logging.debug('[check_cache_dir] current cache dir: {}'.format(path))
    if not path.exists():
        logging.debug('[check_cache_dir] created cached dir: {}'.format(path))
        path.mkdir(parents=True)


def get_badge_path(badge_id: str) -> Path:
    return get_cache_dir().joinpath(badge_id)


def is_cached(badge_id: str) -> bool:
    return get_badge_path(badge_id).exists()


def save_badge(badge_id, content):
    path = get_badge_path(badge_id).as_posix()
    logging.debug('[save_badge] cached badge file {}'.format(path))

    with open(path, 'wb') as fp:
        fp.write(content)


def get_cache_dir() -> Path:
    return Path(os.getcwd()).joinpath("cache")


def cleanup(cache):
    for f in get_cache_dir().iterdir():
        if f.name not in cache.values():
            logging.debug('[cleanup] cleanup {}'.format(f.name))
            f.unlink(missing_ok=True)
