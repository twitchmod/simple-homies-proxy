import dataclasses


@dataclasses.dataclass
class User:
    userId: str
    badgeId: str
