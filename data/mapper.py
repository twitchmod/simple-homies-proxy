from data.model import User


def map_data(badges) -> [User]:
    for badge in badges['badges']:
        yield User(badgeId=badge['badgeId'], userId=badge['userId'])
