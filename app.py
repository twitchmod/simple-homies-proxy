import asyncio
import logging
import threading
import time

import aiohttp
import uvicorn
from fastapi import FastAPI
from starlette.responses import FileResponse

import config
from data.mapper import map_data
from data.model import User
from util import save_badge, is_cached, cleanup, get_badge_path, check_cache_dir

CACHE = {}

logging.basicConfig(level=logging.DEBUG)


async def create_cache_task(session, users: [User]):
    futures = [asyncio.ensure_future(cache_badge_w(session, user)) for user in users]
    return await asyncio.gather(*futures)


async def cache_badge_w(session, user):
    if await cache_badge(session, user):
        return user
    else:
        logging.error('[cache_badge_w] cannot cache: {}'.format(user))
        return None


def is_valid_content(content):
    if not content:
        return False

    if content[:15] == b'<!DOCTYPE html>':
        return False

    return True


async def cache_badge(session, user: User):
    async with session.get(config.HOMIES_BADGE_API.format(badge_id=user.badgeId)) as resp:
        content = await resp.content.read()
        if is_valid_content(content):
            save_badge(user.badgeId, content)
            return True
        else:
            return False


async def get_api_info(session):
    logging.debug('[get_api_info] called')
    response = await session.get(config.HOMIES_API)
    if not response.ok:
        logging.debug('[get_api_info] bad response status" {}'.format(response.status))
        raise Exception("Bad response status: {}".format(response.status))

    json = await response.json()
    if not json:
        logging.debug('[get_api_info] bad json: {}'.format(json))
        raise Exception("Bad json: {}".format(json))

    return map_data(json)


async def cache_task(session, users, max_users_to_request):
    logging.debug('[cache_task] called')
    cache = {}
    q = []
    for user in users:
        if not is_cached(user.badgeId):
            q.append(user)
        else:
            cache[user.userId] = user.badgeId

    rq = q[:max_users_to_request] if max_users_to_request is not None and max_users_to_request > 0 else q
    logging.debug('users to cache: {}'.format(len(rq)))
    cached_users = [i for i in await create_cache_task(session, rq) if i]
    logging.debug('cached users: {}'.format(len(cached_users)))
    cache.update({u.userId: u.badgeId for u in cached_users})

    return cache


async def cleanup_task(cache):
    logging.debug('[cleanup_task] called')
    cleanup(cache)


async def run_cache_task():
    logging.debug('[run_cache_task] called')
    async with aiohttp.ClientSession(headers=config.HEADERS,
                                     connector=aiohttp.TCPConnector(limit=config.AIOHTTP_LIMIT)) as session:
        api = await get_api_info(session)
        cache = await cache_task(session, api, config.MAX_TASK_REQUESTS)
        CACHE.clear()
        CACHE.update(cache)
        logging.debug('[run_cache_task] total: {}'.format(len(CACHE)))
        await session.close()
        await cleanup_task(CACHE)


class CacheTask(threading.Thread):
    def __init__(self):
        super().__init__()
        self.daemon = True

    def run(self) -> None:
        while True:
            print("[CacheTask] Fetching badges...")
            for try_count in range(1, 4):
                try:
                    asyncio.run(run_cache_task())
                    break
                except Exception as e:
                    print("[CacheTask] Error [{}]: {}".format(try_count, e))
                    continue

            print("[CacheTask] Done!")
            time.sleep(config.TIMEOUT)


app = FastAPI()


@app.get("/badge/{badge_id}")
async def get_badge(badge_id: str):
    return FileResponse(get_badge_path(badge_id))


@app.get("/badges")
async def get_cache():
    return CACHE


if __name__ == '__main__':
    check_cache_dir()
    logging.debug('Create and start CacheTask')
    t = CacheTask()
    t.start()
    logging.debug('Running uvicorn [{}]'.format((config.HOST, config.PORT)))
    uvicorn.run(app, host=config.HOST, port=config.PORT)
