HOMIES_API = "https://chatterinohomies.com/api/badges/list"
HOMIES_BADGE_API = "https://chatterinohomies.com/api/badges/{badge_id}?size=3x"

HEADERS = {"Accept-Encoding": "gzip, deflate", "Accept-Language": "ru-RU,en,*", "User-Agent": "chatterino/7.3.5H1 (61816ebd9)"}

CACHE_DIR = "cache"

TIMEOUT = 1 * 60 * 60 * 3
MAX_TASK_REQUESTS = None
AIOHTTP_LIMIT = 50

PORT = 8080
HOST = "127.0.0.2"
